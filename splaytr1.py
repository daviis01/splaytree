#  File splaytr1.py  Used to test splaytree.py
#  
#  Date: 9/17/2011

from splaytree import SplayTree
from person import Person

def main():
   
   t = SplayTree() #1
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.retrieve(1)
   print( t )
   print( str(t) == '(*1(((*2(*3*))4(*5*))6(*7*)))' )
      
   t = SplayTree()#2
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.findMin()
   print( t )
   print( str(t) == '(*1(((*2(*3*))4(*5*))6(*7*)))' )
   
   t = SplayTree()#3
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.retrieve(1)
   t.delete(3)
   print( t )
   print( str(t) == '((*1*)2((*4(*5*))6(*7*)))' )
   
   t = SplayTree()#4
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.retrieve(1)
   t.delete(3)
   t.findMax()
   print( t )
   print( str(t) == '((((*1*)2(*4(*5*)))6*)7*)' )
   print( t.getSize() )

   t = SplayTree() #5
   t.insert(Person('Joe', 25))
   t.insert(Person('Jill',35))
   t.insert(Person('Jon',15))
   t.insert(Person('Jack',25))
   t.insert(Person('John',30))
   t.insert(Person('Jud',95))
   t.insert(Person('Joey',27))
   st = str(t) + '\n\n'
   t.update(Person('James', 25))
   st += str(t) + '\n\n'
   x = t.retrieve(Person('',15))
   st += str(x) + '\n\n'
   st += str(t) + '\n\n'
   x = t.delete(Person('', 35))
   st += str(x) + '\n\n'
   st += str(t) + '\n\n'
   x = t.findMax()
   st += str(x) + '\n\n'
   st += str(t) + '\n\n'
   print( st )
   print( t )
   print( str(t) == '((((*Name: Jon Id: 15 (*Name: James Id: 25 *))Name: Joey Id: 27 *)Name: John Id: 30 *)Name: Jud Id: 95 *)' )
   print( t.getSize() )

   t = SplayTree() #6
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.insert(3.5)
   print( t )
   print( str(t) == '((((*1*)2*)3*)3.5(((*4*)5(*6*))7*))' )
   
   t = SplayTree() #7
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.insert(3.5)
   t.delete(3.5)
   print( t )
   print( str(t) == '(((*1*)2*)3(((*4*)5(*6*))7*))' )
  

   t = SplayTree() #8
   t.insert(3)
   t.insert(2)
   t.insert(1)
   t.delete(1)
   print( t )
   print( str(t) == '(*2(*3*))' )
   print( t.getSize() )


   st = SplayTree() #9
   st.insert(1)
   st.insert(2)
   st.insert(3)
   st.insert(4)
   print( st )
   print( str(st) == '((((*1*)2*)3*)4*)' )
   x = st.retrieve(3.5)
   print( x )
   print( st.getSize() )

   t = SplayTree() #10
   t.insert(Person('Joe', 25))
   x = Person('Jill',35)
   t.insert(x)
   x.name = 'Julie'
   t.insert(Person('Jon',15))
   t.insert(Person('Jack',25))
   t.insert(Person('John',30))
   t.insert(Person('Jud',95))
   t.insert(Person('Joey',27))
   x = t.retrieve(Person('', 30))
   x.id = 10000
   x = t.update(Person('Fred', 25))
   x.name = "James Brown"
   x = t.delete(Person('', 95))
   print( x )
   x.id = 2000
   print( t )
   print( str(t) == '((((*Name: Jon Id: 15 *)Name: Fred Id: 25 *)Name: Joey Id: 27 (*Name: John Id: 30 *))Name: Jill Id: 35 *)' )
   t.inorder()
   
   

if __name__ == '__main__': main()

'''The output, some of it wrapped around!!
(*1(((*2(*3*))4(*5*))6(*7*)))
True
(*1(((*2(*3*))4(*5*))6(*7*)))
True
((*1*)2((*4(*5*))6(*7*)))
True
((((*1*)2(*4(*5*)))6*)7*)
True
6
(((*Name: Jon Id: 15 *)Name: Joe Id: 25 *)Name: Joey Id: 27 ((*Name: John Id: 30 *)Name: Jill Id: 35 (*Name: Jud Id: 95 *)))

((*Name: Jon Id: 15 *)Name: James Id: 25 (*Name: Joey Id: 27 ((*Name: John Id: 30 *)Name: Jill Id: 35 (*Name: Jud Id: 95 *))))

Name: Jon Id: 15 

(*Name: Jon Id: 15 (*Name: James Id: 25 (*Name: Joey Id: 27 ((*Name: John Id: 30 *)Name: Jill Id: 35 (*Name: Jud Id: 95 *)))))

Name: Jill Id: 35 

(((*Name: Jon Id: 15 (*Name: James Id: 25 *))Name: Joey Id: 27 *)Name: John Id: 30 (*Name: Jud Id: 95 *))

Name: Jud Id: 95 

((((*Name: Jon Id: 15 (*Name: James Id: 25 *))Name: Joey Id: 27 *)Name: John Id: 30 *)Name: Jud Id: 95 *)


((((*Name: Jon Id: 15 (*Name: James Id: 25 *))Name: Joey Id: 27 *)Name: John Id: 30 *)Name: Jud Id: 95 *)
True
5
((((*1*)2*)3*)3.5(((*4*)5(*6*))7*))
True
(((*1*)2*)3(((*4*)5(*6*))7*))
True
(*2(*3*))
True
2
((((*1*)2*)3*)4*)
True
None
4
Name: Jud Id: 95 
((((*Name: Jon Id: 15 *)Name: Fred Id: 25 *)Name: Joey Id: 27 (*Name: John Id: 30 *))Name: Jill Id: 35 *)
True
Name: Jon Id: 15 
Name: Fred Id: 25 
Name: Joey Id: 27 
Name: John Id: 30 
Name: Jill Id: 35 
>'''      
   
      
