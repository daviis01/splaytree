'''
File spnode1.py.  Used for testing the splay tree project."
'''

from splaytree import SplayNode, SplayTree
from person import Person

def main():
   
   # 1
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   x = node.rotateLeft()
   print( x )
   print( str(x) == '((((*1*)2(*3*))5*)8*)' )

   print( 'Run # 2' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   x = node.rotateLeftThenRight()
   print( x )
   print( str(x) == '(((*1*)2*)3(*5(*8*)))' )
   
   print( "Run # 3" )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   x =  node.doubleRotateRight()   
   print( x )
   print( str(x) == '(*1(*2((*3*)5(*8*))))' )
   

   print( 'Run # 4' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   node.getRight().setLeft(SplayNode(7))
   node.getRight().setRight(SplayNode(10))
   node.getLeft().getRight().setRight(SplayNode(4))
   node.getRight().getLeft().setLeft(SplayNode(6))
   node = node.find(7)
   x =  node 
   print( x )
   print( str(x) == '((((*1*)2(*3(*4*)))5(*6*))7(*8(*10*)))' )
   
   
   print( 'Run # 5' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   node.getRight().setLeft(SplayNode(7))
   node.getRight().setRight(SplayNode(10))
   node.getLeft().getRight().setRight(SplayNode(4))
   node.getRight().getLeft().setLeft(SplayNode(6))
   node = node.find(6)
   x =  node 
   print( x )
   print( str(x) == '((((*1*)2(*3(*4*)))5*)6(*7(*8(*10*))))' )
   
   print( 'Run # 6' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   node.getRight().setLeft(SplayNode(7))
   node.getRight().setRight(SplayNode(10))
   node.getLeft().getRight().setRight(SplayNode(4))
   node.getRight().getLeft().setLeft(SplayNode(6))
   node.inorder()
   node = node.findMax()
   x =  node 
   print( x )
   print( str(x) == '(((((*1*)2(*3(*4*)))5((*6*)7*))8*)10*)' )
   
   print( 'Run # 7' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   node.getRight().setLeft(SplayNode(7))
   node.getRight().setRight(SplayNode(10))
   node.getLeft().getRight().setRight(SplayNode(4))
   node.getRight().getLeft().setLeft(SplayNode(6))
   node = node.findMin()
   x =  node 
   print( x )
   print( str(x) == '(*1(*2((*3(*4*))5(((*6*)7*)8(*10*)))))' )
   
   print( 'Run # 8' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   node.getRight().setLeft(SplayNode(7))
   node.getRight().setRight(SplayNode(10))
   node.getLeft().getRight().setRight(SplayNode(4))
   node.getRight().getLeft().setLeft(SplayNode(6))
   node = node.getRight().findMin()
   x =  node 
   print( x )
   print( str(x) == '(*6(*7(*8(*10*))))' )
   
   print( 'Run # 9' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   node.getRight().setLeft(SplayNode(7))
   node.getRight().setRight(SplayNode(10))
   node.getLeft().getRight().setRight(SplayNode(4))
   node.getRight().getLeft().setLeft(SplayNode(6))
   node = node.getRight().getLeft().findMax()
   x =  node 
   print( x )
   print( str(x) == '((*6*)7*)' )
   
   print( 'Run # 10' )
   node = SplayNode(Person('Jim', 87))
   f,t = node.insertInNode(Person('Sam', 100)) 
   f,t = t.insertInNode(Person('Sadie', 93))
   print( t )
   print( str(t) == '((*Name: Jim Id: 87 *)Name: Sadie Id: 93 (*Name: Sam Id: 100 *))' )
   t.inorder()
   
   print( 'Run # 11' )
   node = SplayNode(3)
   found, node = node.insertInNode(4)
   found, node = node.insertInNode(1)
   node = node.rotateLeft()
   x =  node
   print( x )
   print( str(x) == '((*1*)3(*4*))' )
   
   print( 'Run # 12 ' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   node.getRight().setLeft(SplayNode(7))
   node.getRight().setRight(SplayNode(10))
   node.getLeft().getRight().setRight(SplayNode(4))
   node.getRight().getLeft().setLeft(SplayNode(6))
   node.left.left.left = SplayNode(0)
   node.left.left.right = SplayNode(1.5)
   node.right.right.right = SplayNode(11)
   node.right.right.left = SplayNode(9)
   x =  node
   print( x )
   print( str(x) == '((((*0*)1(*1.5*))2(*3(*4*)))5(((*6*)7*)8((*9*)10(*11*))))' )
   
   print( 'Run #13' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   node.getRight().setLeft(SplayNode(7))
   node.getRight().setRight(SplayNode(10))
   node.getLeft().getRight().setRight(SplayNode(4))
   node.getRight().getLeft().setLeft(SplayNode(6))
   node.left.left.left = SplayNode(0)
   node.left.left.right = SplayNode(1.5)
   node.right.right.right = SplayNode(11)
   node.right.right.left = SplayNode(9)
   x =  node.doubleRotateLeft()
   print( x )
   print( str(x) == '((((((*0*)1(*1.5*))2(*3(*4*)))5((*6*)7*))8(*9*))10(*11*))' )
   
   print( 'Run #14' )
   node = SplayNode(5)
   node.setLeft(SplayNode(2))
   node.setRight(SplayNode(8))
   node.getLeft().setLeft(SplayNode(1))
   node.getLeft().setRight(SplayNode(3))
   node.getRight().setLeft(SplayNode(7))
   node.getRight().setRight(SplayNode(10))
   node.getLeft().getRight().setRight(SplayNode(4))
   node.getRight().getLeft().setLeft(SplayNode(6))
   node.left.left.left = SplayNode(0)
   node.left.left.right = SplayNode(1.5)
   node.right.right.right = SplayNode(11)
   node.right.right.left = SplayNode(9)
   x =  node.doubleRotateRight()
   print( x )
   print( str(x) == '((*0*)1((*1.5*)2((*3(*4*))5(((*6*)7*)8((*9*)10(*11*))))))' )
   

if __name__ == '__main__': main()

''' The runs:
((((*1*)2(*3*))5*)8*)
True
Run # 2
(((*1*)2*)3(*5(*8*)))
True
Run # 3
(*1(*2((*3*)5(*8*))))
True
Run # 4
((((*1*)2(*3(*4*)))5(*6*))7(*8(*10*)))
True
Run # 5
((((*1*)2(*3(*4*)))5*)6(*7(*8(*10*))))
True
Run # 6
1
2
3
4
5
6
7
8
10
(((((*1*)2(*3(*4*)))5((*6*)7*))8*)10*)
True
Run # 7
(*1(*2((*3(*4*))5(((*6*)7*)8(*10*)))))
True
Run # 8
(*6(*7(*8(*10*))))
True
Run # 9
((*6*)7*)
True
Run # 10
((*Name: Jim Id: 87 *)Name: Sadie Id: 93 (*Name: Sam Id: 100 *))
True
Name: Jim Id: 87 
Name: Sadie Id: 93 
Name: Sam Id: 100 
Run # 11
((*1*)3(*4*))
True
Run # 12 
((((*0*)1(*1.5*))2(*3(*4*)))5(((*6*)7*)8((*9*)10(*11*))))
True
Run #13
((((((*0*)1(*1.5*))2(*3(*4*)))5((*6*)7*))8(*9*))10(*11*))
True
Run #14
((*0*)1((*1.5*)2((*3(*4*))5(((*6*)7*)8((*9*)10(*11*))))))
True
'''    
    
