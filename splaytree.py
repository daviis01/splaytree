'''
 File: splaytree.py
 Author(s): Steve Hubbard and Isaac Davis
 Date: 7/10/13
 Description: This module implements the SplayTree class and the
   SplayNode class.  The classes use bottom up splaying rather than 
   top down splaying.  We do not allow duplicate objects in the tree.
'''

from person import Person
from copy import deepcopy
from mystack import MyStack

#node class
class SplayNode:
   ''' This module implements the SplayNode class. This
     class in turn is used by the SplayTree class.  The classes
     use bottom up splaying rather than top down splaying.  We
     do not allow duplicate objects in the tree.
   '''
   
   def __init__(self, item, left = None, right = None):
      self.left = left
      self.item = item
      self.right = right

   def __str__(self): 
      st = '('
      if (self.left == None):
         st += '*'
      else:
         st += str(self.left) # recursion
      st += str(self.item)
      if (self.right == None):
         st += '*'
      else:
         st += str(self.right) # recursion
      st += ')'
      return st

#boring accessors
   def getLeft(self):
      return self.left

   def getRight(self):
      return self.right
	
   def getItem(self):
      return self.item
	
#boring mutators
   def setItem(self, anItem):
      self.item = anItem

   def setLeft(self, aNode):
      self.left = aNode

   def setRight(self, aNode):
      self.right = aNode

#need to implicitly compare between objects and base elements
#In hindsight this is not the most "correct" way to do this
#instead there should only be comparisons between splaynodes and 
#convert people and ints into splay nodes before comparing.
#this is just a patch beacuse I dont want to rewrite all the methods.
   def __le__(self, other):
      if type(other) == SplayNode:
         return self.item <= other.item
      #elif type(other) == Person and type(self.item) == Person:
      #   return self.item.getId() <= other.getId()
      else:
         return self.item <= other
   
   def __lt__(self, other):
      if type(other) == SplayNode:
         return self.item < other.item
      #elif type(other) == Person and type(self.item) == Person:
      #   return self.item.getId() < other.getId()
      else:
         return self.item < other

   def __eq__(self, other):
      if type(other) == SplayNode:
         return self.item == other.item
      #elif type(other) == Person and type(self.item) == Person:
      #   return self.item.getId() == other.getId()
      else:
         return self.item == other

   def __gt__(self, other):
      if type(other) == SplayNode:
         return self.item > other.item
      #elif type(other) == Person and type(self.item) == Person:
      #   return self.item.getId() > other.getId()
      else:
         return self.item > other

   def __ge__(self, other):
      if type(other) == SplayNode:
         return self.item >= other.item
      #elif type(other) == Person and type(self.item) == Person:
      #   return self.item.getId() >= other.getId()
      else:
         return self.item >= other

#exciting things to do with nodes   
   def inorder(self):
      ''' Perform an inorder traversal of the subtree rooted at
        the receiver.  Print each item in this subtree during
        the traversal.  This is done with recursion.
      '''
      if self == None:
         return
      else:
         if self.left != None:
            self.left.inorder()
         if self.item != None:
            print (self.item)
         if self.right != None:          
            self.right.inorder()         
      
   def insertInNode(self, anItem):
      ''' Try to insert a copy of anItem into the bottom up splay
       tree rooted at the receiver.  If anItem is already in the tree,
       do not insert an extra copy. In any case, splay the new node,
       or the last node on the search path, to the root.  The method
       will answer a tuple.  The first element is True or False
       according to whether a new element was added or not.  The
       second element is the new root node.
       '''
      isAdded = True
      aStack = MyStack()
      currentNode = self
      while currentNode != None:
         if currentNode == anItem:
            isAdded = False
         aStack.push(currentNode)
         aBool = (currentNode > anItem)
         if currentNode > anItem:
            currentNode = currentNode.left
         else:
            currentNode = currentNode.right
      if isAdded:
         currentNode = aStack.top()
         newNode = deepcopy(anItem)
         if not isinstance(newNode, SplayNode):
            newNode = SplayNode(newNode)
         if currentNode > anItem:
            currentNode.left = newNode
         else:
            currentNode.right = newNode
         aStack.push(newNode)
      return isAdded, self.splayToRoot(aStack)

   def find(self, anItem):
      '''This is used to find an item. It will answer a node that will
      be that root of a tree after calling splayToRoot.
      '''
      aStack = MyStack()
      currentItem = self
      while currentItem != None and currentItem.item != anItem:
         aStack.push(currentItem)
         if currentItem.item > anItem:
            currentItem = currentItem.left
         else:
            currentItem = currentItem.right
      aBool = currentItem == anItem
      if currentItem == anItem:
         aStack.push(currentItem)
      return self.splayToRoot(aStack)

   def findMax(self):
      '''Find the max of the subtree below the node that it was called on.
      This will be the max of the tree if called on the root of the tree.
      '''
      aStack = MyStack()
      currentNode = self
      while currentNode != None:
         aStack.push(currentNode)
         currentNode = currentNode.right    
      return self.splayToRoot(aStack)

   def findMin(self):
      '''Find the min of the subtree of the node it was called on. This will
      be the min of the whole tree if called on the root node.
      '''
      aStack = MyStack()
      currentNode = self
      while currentNode != None:
         aStack.push(currentNode)
         currentNode = currentNode.left   
      return self.splayToRoot(aStack)
      

#very important to the heavy lifting
   def splayToRoot(self, stack):
      '''  Perform a bottom up splay beginning at the node at the
       top of the stack.  Answer the root of the new tree.
      '''
      oldRoot = None
      if not stack.isEmpty():
         child = stack.pop()
      while not stack.isEmpty():
         if not stack.isEmpty():
            parent = stack.pop()
            #double rotations
            if not stack.isEmpty():
               grandParent = stack.pop()
               if not stack.isEmpty():
                  oldRoot = stack.top()
               else:
                  oldRoot = None
               if grandParent.item > parent.item:
                  if parent.item > child.item:
                     grandParent.doubleRotateRight()
                     if oldRoot != None and oldRoot > child:
                        oldRoot.left = child
                     else:
                        if oldRoot != None:
                           oldRoot.right = child
                  else:
                     grandParent.rotateLeftThenRight()
                     if oldRoot != None and oldRoot > child:
                        oldRoot.left = child
                     else:
                        if oldRoot != None:
                           oldRoot.right = child
               else:
                  if parent.item > child.item:
                     grandParent.rotateRightThenLeft()
                     if oldRoot != None and oldRoot > child:
                        oldRoot.left = child
                     else:
                        if oldRoot != None:
                           oldRoot.right = child
                  else:
                     grandParent.doubleRotateLeft()
                     if oldRoot != None and oldRoot > child:
                        oldRoot.left = child
                     else:
                        if oldRoot != None:
                           oldRoot.right = child
            #single rotation else if ladder
            else:
               if parent.item > child.item:
                  parent.rotateRight()
               else:
                  parent.rotateLeft()
      return child
   
#for all rotations the node that the current root of the rotations is attached to 
#will have to be reattached to the new node, to be done in the tree methods

#single rotations
   def rotateLeft(self):
      '''  Perform a left rotation of the subtree rooted at the
       receiver.  Answer the root node of the new subtree.  
      '''
      child = self.right
      if (child == None):
         print( 'Error!  No right child in rotateLeft. ' )
         return None  # redundant
      else:
         self.right = child.left
         child.left = self
         return child

   def rotateRight(self):
      ''' Perform a right rotation of the subtree rooted at the 
      receiver. Answer the root node of the new subtree.
      '''
      child = self.left
      if (child == None):
         print("Error in node.RotateRight no left child")
         return None
      else:
         self.left = child.right
         child.right = self
         return child

#double rotations
   def rotateLeftThenRight(self):
      ''' Perform a set of rotations through assignment statements. 
      This set is to do a "left" first then a "right". Answer the "root".
      '''
      child = self.left
      grandChild = child.right
      child.right = grandChild.left
      self.left = grandChild.right
      grandChild.left = child
      grandChild.right = self
      return grandChild
      

   def rotateRightThenLeft(self):
      ''' Perform a set of rotations through assignment statements. 
      This set is to do a "right" first then a "left". Answer the "root".
      '''
      child = self.right
      grandchild = child.left
      child.left = grandchild.right
      self.right = grandchild.left
      grandchild.right = child
      grandchild.left = self
      return grandchild
      
      
   def doubleRotateRight(self):
      ''' Perform a set of rotations through assignment statements. 
      This set is to do a "right" first then a "right" again. Answer the "root".
      '''
      child = self.left
      grandChild = child.left
      self.left = child.right
      child.left = grandChild.right
      child.right = self
      grandChild.right = child
      return grandChild
   

   def doubleRotateLeft(self):
      ''' Perform a set of rotations through assignment statements. 
      This set is to do a "left" first then a "left" again. Answer the "root".
      '''
      child = self.right
      grandChild = child.right
      self.right = child.left
      child.right = grandChild.left
      child.left = self
      grandChild.left = child
      return grandChild

      
#the splay tree class
class SplayTree:
   
   def __init__(self):
      self.size = 0
      self.root = None

   def __str__(self):
      if self.root != None:
         return str(self.root)
      else:
         return ""
   
   def delete(self, anItem):
      '''  Atempt to find a match (==) for anItem in the receiver.
      If found, splay the corresponding node to the root and answer
      the item of the node. If not found, splay the last node on
      the search path to the root. In this case, answer None.  If
      found, we remove the node and make the largest element of the
      new left subtree (from the splaying of the node to the root
      position) the new root node of the tree.  Of course finding
      the largest element uses a splaying on that left subtree.
      If there is no left subtree, the right subtree becomes the
      root.  This may leave us with an empty tree.  If found,
      decrement the size of the tree and answer the item deleted.
      If not found, answer None.
      '''
      self.root = self.root.find(anItem)
      if self.root != anItem:
         return None
      else:
         self.size -= 1
         foundNode = self.root
         if self.root.getLeft() == None:
            self.root = self.root.getRight()
            return deepcopy(foundNode.getItem())
         else:
            lst = self.root.getLeft()
            lstInOrder = lst.findMax()
            lstInOrder.setRight(self.root.getRight())
            self.root = lstInOrder
            return deepcopy(foundNode.getItem())

   def findMax(self):
      ''' Find the largest element in the splay tree.  Splay that
      element to the root.  Answer a deep copy of the element.
      If the tree is empty, answer None.
      '''
      if self.root == None:
         return None
      self.root = self.root.findMax()
      return deepcopy(self.root.getItem())

   def findMin(self):
      ''' Find the smallest element in the splay tree.  Splay that
      element to the root.  Answer a deep copy of the element.  If
      the tree is empty, answer None.
      '''
      if (self.root == None):
         return None
      self.root = self.root.findMin()
      return deepcopy(self.root.getItem())

   def getSize(self):
      return self.size

   def inorder(self):
      ''' Print the contents of the receiver, in inorder.
        Print one item per line.
      '''
      if self.root != None:
         self.root.inorder()

   def insert(self, anItem):
      ''' Insert a deep copy of anItem into the bottom up splay tree.
      If anItem is already present in the tree, do not insert a new 
      copy of anItem.  If anItem is added, increment the size of
      the receiver.  In either case, we splay from
      the last node.  If anItem was added, answer anItem.  If not,
      answer None.
      '''
      newItem = (deepcopy(anItem))
      if not isinstance(newItem, SplayNode):
         newItem = SplayNode(newItem)
      if self.root == None:
         self.root = newItem
         self.size += 1
         return anItem
      else:
         wasAdded, newRoot = self.root.insertInNode(newItem)
         self.root = newRoot
         if wasAdded:
            self.size += 1
            return anItem
         else:
            return None
      
         
   def retrieve(self, anItem):
      ''' I think this function is just susposed to make the root of the tree
      anItem and return a deepcopy of anItem. But I could be wrong
      '''
      self.root = self.root.find(anItem)
      if self.root == anItem:
         return deepcopy(self.root.getItem())
      else:
         return None

   def update(self, anItem):
      '''
      Update does a find of anItem, if it is there it replaces it with itself then returns a 
      deep copy of itself, if not then it returns None
      '''
      self.root = self.root.find(anItem)
      if self.root == anItem:
         self.root.setItem(anItem)
         return deepcopy(self.root.getItem())
      else:
         return None


def main():
   
   print('My name is Isaac Davis')
   print('Test the SplayNode class: ')
   
   a = SplayNode(20, SplayNode(10), SplayNode(25))
   b = SplayNode(40, SplayNode(35), SplayNode(45))
   c = SplayNode(30, a, b)
   x = c.rotateLeft()
   print( x )
   print( str(x) == '((((*10*)20(*25*))30(*35*))40(*45*))' )
   print( '' )

   a = SplayNode(20, SplayNode(10), SplayNode(25))
   b = SplayNode(40, SplayNode(35), SplayNode(45))
   c = SplayNode(30, a, b)
   x = c.rotateRight()
   print( x )
   print( str(x) == '((*10*)20((*25*)30((*35*)40(*45*))))' )
   print( '' )
   
   a = SplayNode(20, SplayNode(10), SplayNode(25))
   b = SplayNode(40, SplayNode(35), SplayNode(45))
   c = SplayNode(30, a, b)
   d = SplayNode(60, SplayNode(55), SplayNode(65))
   e = SplayNode(90, SplayNode(80), SplayNode(100))
   f = SplayNode(70, d, e)
   root = SplayNode(50, c, f)
   print( root )
   print( '' )

   a = SplayNode(20, SplayNode(10), SplayNode(25))
   b = SplayNode(40, SplayNode(35), SplayNode(45))
   c = SplayNode(30, a, b)
   d = SplayNode(60, SplayNode(55), SplayNode(65))
   e = SplayNode(90, SplayNode(80), SplayNode(100))
   f = SplayNode(70, d, e)
   root = SplayNode(50, c, f)
   x = root.rotateRightThenLeft()
   print( x )
   print( str(x) ==  \
   '(((((*10*)20(*25*))30((*35*)40(*45*)))50(*55*))60((*65*)70((*80*)90(*100*))))' )
   print( '' )

   a = SplayNode(20, SplayNode(10), SplayNode(25))
   b = SplayNode(40, SplayNode(35), SplayNode(45))
   c = SplayNode(30, a, b)
   d = SplayNode(60, SplayNode(55), SplayNode(65))
   e = SplayNode(90, SplayNode(80), SplayNode(100))
   f = SplayNode(70, d, e)
   root = SplayNode(50, c, f)
   x = root.rotateLeftThenRight()
   print( x )
   print( str(x) ==  \
   '((((*10*)20(*25*))30(*35*))40((*45*)50(((*55*)60(*65*))70((*80*)90(*100*)))))' )
   print( '' )

   a = SplayNode(20, SplayNode(10), SplayNode(25))
   b = SplayNode(40, SplayNode(35), SplayNode(45))
   c = SplayNode(30, a, b)
   d = SplayNode(60, SplayNode(55), SplayNode(65))
   e = SplayNode(90, SplayNode(80), SplayNode(100))
   f = SplayNode(70, d, e)
   root = SplayNode(50, c, f)
   x = root.doubleRotateLeft()
   print( x )
   print( str(x) ==  \
   '((((((*10*)20(*25*))30((*35*)40(*45*)))50((*55*)60(*65*)))70(*80*))90(*100*))' )
   print( '' )
   
   a = SplayNode(20, SplayNode(10), SplayNode(25))
   b = SplayNode(40, SplayNode(35), SplayNode(45))
   c = SplayNode(30, a, b)

   d = SplayNode(60, SplayNode(55), SplayNode(65))
   e = SplayNode(90, SplayNode(80), SplayNode(100))
   f = SplayNode(70, d, e)
   root = SplayNode(50, c, f)
   x = root.doubleRotateRight()
   print( x )
   print( str(x) == \
   '((*10*)20((*25*)30(((*35*)40(*45*))50(((*55*)60(*65*))70((*80*)90(*100*))))))' )
   print( '' )
   
   a = SplayNode(20, SplayNode(10), SplayNode(25))
   b = SplayNode(40, SplayNode(35), SplayNode(45))
   c = SplayNode(30, a, b)
   d = SplayNode(60, SplayNode(55), SplayNode(65))
   e = SplayNode(90, SplayNode(80), SplayNode(100))
   f = SplayNode(70, d, e)
   root = SplayNode(50, c, f)
   x = root.find(35)
   print( x )
   print( str(x) == \
   '((((*10*)20(*25*))30*)35((*40(*45*))50(((*55*)60(*65*))70((*80*)90(*100*)))))')

   print('Test the SplayTree class: ')
   t = SplayTree()
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.retrieve(1)
   print( str(t) == '(*1(((*2(*3*))4(*5*))6(*7*)))')
   print( 'The size of the tree is ' + str(t.getSize()) )
   
   t = SplayTree()
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.findMin()
   print( str(t) ==  '(*1(((*2(*3*))4(*5*))6(*7*)))')
   
   t = SplayTree()
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.retrieve(1)
   t.delete(3)
   print( str(t) ==  '((*1*)2((*4(*5*))6(*7*)))' )
   
   t = SplayTree()
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.retrieve(1)
   t.delete(3)
   t.findMax()
   print( str(t) == '((((*1*)2(*4(*5*)))6*)7*)')

   t = SplayTree()
   t.insert(Person('Joe', 25))
   t.insert(Person('Jill',35))
   t.insert(Person('Jon',15))
   t.insert(Person('Jack',25))
   t.insert(Person('John',30))
   t.insert(Person('Jud',95))
   t.insert(Person('Joey',27))
   st = str(t) + '\n'
   t.update(Person('James', 25))
   st += str(t) + '\n'
   x = t.retrieve(Person('',15))
   st += str(x) + '\n'
   st += str(t) + '\n'
   x = t.delete(Person('', 35))
   st += str(x) + '\n'
   st += str(t) + '\n'
   x = t.findMax()
   st += str(x) + '\n'
   st += str(t) + '\n'
   print( t )

   print( 'The size of the tree is ' + str(t.getSize()) )

   t = SplayTree()
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.insert(3.5)
   print( str(t) == '((((*1*)2*)3*)3.5(((*4*)5(*6*))7*))' )
   
   t = SplayTree()
   t.insert(1)
   t.insert(2)
   t.insert(3)
   t.insert(4)
   t.insert(5)
   t.insert(6)
   t.insert(7)
   t.insert(3.5)
   t.delete(3.5)
   print( str(t) == '(((*1*)2*)3(((*4*)5(*6*))7*))')
   print( 'The size of the tree is ' + str(t.getSize()) )
   

   t = SplayTree()
   t.insert(3)
   t.insert(2)
   t.insert(1)
   t.delete(1)
   print( 'The size of the tree is ' + str(t.getSize()) )

   t = SplayTree()
   t.insert(Person('Joe', 25))
   t.insert(Person('Jill',35))
   t.insert(Person('Jon',15))
   t.insert(Person('Jack',25))
   t.insert(Person('John',30))
   t.insert(Person('Jud',95))
   t.insert(Person('Joey',27))
   t.inorder()
      
if __name__ == '__main__': main()

''' Output, from splaytree.py, wrapped around!
[evaluate splaytree.py]
My name is 
Test the SplayNode class: 
((((*10*)20(*25*))30(*35*))40(*45*))
True

((*10*)20((*25*)30((*35*)40(*45*))))
True

((((*10*)20(*25*))30((*35*)40(*45*)))50(((*55*)60(*65*))70((*80*)90(*100*))))

(((((*10*)20(*25*))30((*35*)40(*45*)))50(*55*))60((*65*)70((*80*)90(*100*))))
True

((((*10*)20(*25*))30(*35*))40((*45*)50(((*55*)60(*65*))70((*80*)90(*100*)))))
True

((((((*10*)20(*25*))30((*35*)40(*45*)))50((*55*)60(*65*)))70(*80*))90(*100*))
True

((*10*)20((*25*)30(((*35*)40(*45*))50(((*55*)60(*65*))70((*80*)90(*100*))))))
True

((((*10*)20(*25*))30*)35((*40(*45*))50(((*55*)60(*65*))70((*80*)90(*100*)))))
True
Test the SplayTree class: 
True
The size of the tree is 7
True
True
True
((((*Name: Jon Id: 15 (*Name: James Id: 25 *))Name: Joey Id: 27 *)Name: John Id: 30 *)Name: Jud Id: 95 *)
The size of the tree is 5
True
True
The size of the tree is 7
The size of the tree is 2
Name: Jon Id: 15 
Name: Joe Id: 25 
Name: Joey Id: 27 
Name: John Id: 30 
Name: Jill Id: 35 
Name: Jud Id: 95 

'''
   
      
   
      
